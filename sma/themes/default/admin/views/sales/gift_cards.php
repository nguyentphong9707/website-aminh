<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Ghi chú</h4>
        </div>
        <div class="modal-body">

            <div class="form-group">
                <form id="my-form" method="POST">
                    <textarea id="note" class="form-control skip"><?php echo $note[0]['note']; ?></textarea>
                </form>
            </div>

        </div>
    </div>
</div>
