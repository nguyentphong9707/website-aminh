<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<script>
    $(document).ready(function () {
        oTable = $('#FBData').dataTable({
            "bAutoWidth": false,
            "aaSorting": [[4, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?=lang('all')?>"]],
            "iDisplayLength": 100,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?=admin_url('Ajaxsearch/getKhachhangFB'); ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>",
                });
                aoData.push({
                    "name": "status",
                    "value": "<?=$this->input->get('status');?>",
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                nRow.id = aData[0];
                $('td', nRow).css('background', aData[8] != '' ? aData[8] : '#fff');
                $('td', nRow).css('color', aData[9] != '' ? aData[9] : '#000');
                return nRow;
            },
            "aoColumns": [{"bSortable": false,"mRender": checkbox, "sWidth": "5%"}, 
            {
				"mRender": function (data, type, row) {
                    var arr = data.split(",");
					return "<a href=\"#\" class='clicktoCopy'>"+arr[arr.length - 1]+"</a>";
                }
			},
            {
                "mRender": function (data, type, row) {
                    var arr = data.split(",");
					return "<a href='https://pages.fm/"+ row[12] + "?c_id="+ row[12] + "_"+ row[11] + "'>"+ data + "</a>";
                }
            }, null,
            {
				"mRender": function (data, type, row) {
                    var arr = data.split(",");
					return arr[arr.length - 1];
                }
			},
            {
				"mRender": function (data, type, row) {
						return "<textarea data-id='"+row[0]+"' id='fb-note'>"+data+"</textarea>";
                }
			}, 
            null, null,
            {
                "mData": 13
            },
            {
                "mData": 14
            }
            ]
        });

        $('#FBData').on('change', '#fb-note', function (e) {
            var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>',
            csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';

			e.preventDefault();
			var fbnoteinput = $(this);
			var data = fbnoteinput.val();
			var id = fbnoteinput.attr('data-id');
            var dataJson = { [csrfName]: csrfHash, data:data, id:id };
			$.ajax({
				type: "POST",
				url: "<?php echo admin_url('ajaxsearch/saveFBNote'); ?>",
				data: dataJson,
				cache: false,
				success: function(html)
				{
				}
			});
			return false;
		});

        $('#FBData').on('click', '.clicktoCopy', function (e) {
            e.preventDefault();
            var $tempElement = $("<input>");
            $("body").append($tempElement);
            $tempElement.val($(this).closest(".clicktoCopy").text()).select();
            document.execCommand("Copy");
            $tempElement.remove();
            alert('Copied!!!');
		});

        $('#FBData').on('click', '.clickToCall', function (e) {
            var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>',
            csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
            e.preventDefault();

            var fbcallinput = $(this);
            var id = fbcallinput.attr('data-id');
            var href = fbcallinput.attr('data-href');
            var dataJson = {id:id };
            $.ajax({
                type: "GET",
                url: "<?php echo admin_url('ajaxsearch/saveCallClick'); ?>",
                data: dataJson,
                cache: false,
                success: function(html)
                { 
                    window.location = href;
                }
            });
        });

    });

</script>

<?php if ($Owner || $GP['bulk_actions']) {
            echo admin_form_open('sales/sale_actions', 'id="action-form"');
        }
?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i
                class="fa-fw fa fa-heart"></i>Danh sách khách hàng FB
        </h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12 button-filter">
                <?php
                    $status = $this->input->get('status');
                    $select = "";
                    foreach ($this->data['select'] as $row) {
                        $select .= "<a class='btn btn-primary". ($status == $row['id'] ? " btn-info salebtn-active" : "")  ."' href='". admin_url('sales?status=') .$row['id'] ."' >". $row['status'] ."</a>";
                    }
                    echo $select;
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="FBData" class="table table-bordered table-hover table-striped" cellpadding="0" cellspacing="0" border="0">
                        <thead>
                        <tr>
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkft" type="checkbox" name="check"/>
                            </th>
                            <th>Phone</th>
                            <th>Tên</th>
                            <th>Ngày đăng ký</th>
                            <th>Ngày cuối NT</th>
                            <th>Note</th>
                            <th>Số lần gọi</th>
                            <th>Tin nhắn cuối cùng</th>
                            <th>Call</th>
                            <th>Trạng thái</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="12" class="dataTables_empty"><?= lang('loading_data'); ?></td>
                        </tr>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkft" type="checkbox" name="check"/>
                            </th>
                            <th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($Owner || $GP['bulk_actions']) {
                    ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?=form_submit('performAction', 'performAction', 'id="action-form-submit"')?>
    </div>
    <?=form_close()?>
<?php
                }
?>
