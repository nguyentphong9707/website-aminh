<?php
function showStatus($id, $status){
    $q = $this->statussdt_model->getData();

    $select = "<select id='sl-stt' data-id='$1'>";
    
    foreach ($q as $row) {
        if($status == $row['code']){
            $selected = "selected";
        }else{
            $selected = "";
        }
        $select .= "<option value='". $row['id'] ."' ".  $selected .">". $row['status'] ."</option>";
    }
    
    $select .= "</select>";

    return $select;
}
