<?php

defined('BASEPATH') or exit('No direct script access allowed');

class AjaxSearch extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        if ($this->Customer || $this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER['HTTP_REFERER']);
        }
        $this->lang->admin_load('customers', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('companies_model');
        $this->load->admin_model('note_model');
        $this->load->admin_model('khachhangfb_model');
        $this->load->admin_model('statussdt_model');
        $this->load->admin_model('callhistory_model');
        $this->load->admin_model('myhub_model');
    }

    public function getCustomer()
    {
        $q = $this->input->get('q');
        $rows = $this->companies_model->searchCompanyByPhoneAndName($q);
        if($rows && count($rows) > 0){
            $output = '<ul id="country-list">';
            foreach($rows as $row){
                $output .= '<li><span class="name">'. $row["name"] . '</span><span class="company">' . $row["company"] . '</span>';
                foreach($row['sale'] as $sale){
                    $output .= '<span class="sale">' . $this->sma->formatMoney($sale['total']) . '</span>';
                }
                $output .= '</li>';
            }
            $output .= '</ul>';
            echo $output;
        }else{
            echo '';
        }
        
    }

    public function saveNote()
    {
        $q = $this->input->post('q');
        $rows = $this->note_model->updateNote(1, $q);
        echo '';
    }

    public function getKhachhangFB(){
        $this->sma->checkPermissions('index');
        $this->load->library('datatables');

        $data = $this->myhub_model->getMyhub();

        if($this->input->post('status') && $this->input->post('status') != '27'){
            $this->datatables
            ->select('khachhangfb.id, phone, name, date, date_phone, khachhangfb.note, call_click, lastmsg, color_sdt.background, color_sdt.color, statussdt.status, id_user, id_page')
            ->from('khachhangfb')
            ->join('color_sdt', 'khachhangfb.call_click = color_sdt.count', 'left')
            ->join('statussdt', 'khachhangfb.status = statussdt.id', 'left')
            ->where('phone !=', '')
            ->where('khachhangfb.status', $this->input->post('status'));
        //->unset_column('id');
        }else{
            $this->datatables
                ->select('khachhangfb.id, phone, name, date, date_phone, khachhangfb.note, call_click, lastmsg, color_sdt.background, color_sdt.color, statussdt.status, id_user, id_page')
                ->from('khachhangfb')
                ->join('color_sdt', 'khachhangfb.call_click = color_sdt.count', 'left')
                ->join('statussdt', 'khachhangfb.status = statussdt.id', 'left')
                ->where('phone !=', '');
        }
        $q = $this->statussdt_model->getData();

        $call = '';

        foreach ($data as $index => $dat) {
            $call .= "<a href='#' class='btn btn-primary clickToCall' data-id='$1' data-href='" . $dat['token1'] . "$2" . $dat['token2'] . "'>Sim " . ($index + 1) . "</a>";
        }

        $this->datatables->add_column('Call', $call, 'khachhangfb.id, phone');

        $select = '$2';
        $select .= "<select id='sl-stt' data-id='$1'>";
        foreach ($q as $row) {
            if($q == $row['code']){
                $selected = "selected";
            }else{
                $selected = "";
            }
            $select .= "<option value='". $row['id'] ."' ".  $selected .">". $row['status'] ."</option>";
        }
        
        $select .= "</select>";
        
        $this->datatables->add_column('Trạng thái', $select, 'khachhangfb.id, statussdt.status');

        echo $this->datatables->generate();
    }

    public function saveFBNote()
    {
        $id = $this->input->post('id');
        $data = $this->input->post('data');
        $rows = $this->khachhangfb_model->updateNote($id, $data);
        echo '';
    }

    public function saveCallClick()
    {
        $id = $this->input->get('id');
        $rows = $this->khachhangfb_model->updateCallClick($id);
        $row = $this->callhistory_model->addCallHistory($id);
        echo '';
    }

} 