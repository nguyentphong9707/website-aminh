<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Note_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function updateNote($id = 1, $q)
    {
        $data['note'] = $q;
        if ($this->db->update('note', $data, ['id' => $id])) {
            return true;
        }
        return false;
    }

    public function getNote()
    {
        $this->db->select("id, note", false);
        $this->db->from("note");

        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            foreach (($q->result_array()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

}