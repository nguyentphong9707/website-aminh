<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Callhistory_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function addCallHistory($id)
    {
        $data = array(
            'id_khachhangfb' => $id,
            'calltime' =>date('Y-m-d H:i:s')
            );
        $this->db->insert('callhistory', $data);
    }

}