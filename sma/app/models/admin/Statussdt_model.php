<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Statussdt_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getData()
    {
        $data = array();
        $q = $this->db->query('SELECT * FROM sma_statussdt');
        if ($q->num_rows() > 0) {
            foreach (($q->result_array()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return $data;
    }

    public function getByCode($code)
    {
        return "SELECT * FROM sma_statussdt WHERE code = '" . $code . "'";
        // $data = array();
        // $q = $this->db->query('SELECT * FROM sma_statussdt WHERE code = "' . $code . "'");
        // if ($q->num_rows() > 0) {
        //     return $q->result_array()[0];
        // }
        // return $data;
    }

}
