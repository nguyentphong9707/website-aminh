<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Myhub_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getMyhub()
    {
        $q = $this->db->select("myhub.id, name, sdt, token1, token2", false);
        $q = $this->db->from("myhub");

        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            foreach (($q->result_array()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

}