<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Khachhangfb_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function updateNote($id, $q)
    {
        $data['note'] = $q;
        if ($this->db->update('khachhangfb', $data, ['id' => $id])) {
            return true;
        }
        return false;
    }

    public function updateCallClick($id)
    {
        $q = $this->db->get_where('khachhangfb', ['id' => $id], 1);
        if ($q->num_rows() > 0) {
            $call_click = $q->row()->call_click;
            $call_click = $call_click + 1;
            $data['call_click'] = $call_click;
            if ($this->db->update('khachhangfb', $data, ['id' => $id])) {
                return true;
            }
        }
        
        return false;
    }

}